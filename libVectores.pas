unit libVectores;

interface
    {uses TipoRegistro;}
    const
        _MIN = 1;
        _MAX = 50;
        _POS_NULA = Pred(_MIN);
    //end const's
    type
        tipoBase = TRegistro; //declarado en la libreria de TipoRegistro
        tipoIndice = _MIN.._MAX;
        tipoPosicion= _POS_NULA.._MAX;

        TVector = array[tipoIndice] of tipoBase;
    //end type´s
    Procedure crearEstructuraVacia(V:TVector; var ultimo:tipoPosicion);
    Procedure insertarElemento(var V:TVector; var ultimo:tipoPosicion; elem:tipoBase);
    Function  recuperarElemento(var V:TVector; ultimo:tipoPosicion; pos:tipoPosicion):tipoBase;
    Function  primerElemento(V:TVector; ultimo:tipoPosicion):tipoPosicion;
    Function  ultimoElemento(V:TVector; ultimo:tipoPosicion):tipoPosicion;
    Procedure eliminarElemento(var V:TVector; var ultimo:tipoPosicion; pos:tipoPosicion);
    Procedure buscarElemento(V:TVector; ultimo:tipoPosicion; elem:tipoBase; var enc:boolean; var pos: tipoPosicion);
    {buscarElemento, es secuencial y no importa si el velctor esta ordenado}
    Procedure ordenamientoBurbuja(var V:TVector; ultimo:tipoPosicion);
    Procedure ordenamientoBurbujaMejorado(var V:TVector; ultimo:tipoPosicion);
    Function  posMayor(V:TVector; ultimo:tipoPosicion):tipoPosicion;
    Procedure seleccion (var V:TVector; ultimo:tipoPosicion);

    Procedure busquedaBinaria(V:TVector; ultimo:tipoPosicion; elem:tipoBase; var enc:boolean; var pos:tipoPosicion);
    Procedure insertarOrdenado(var V:TVector; var ultimo:tipoPosicion; elem:tipoBase; pos:tipoPosicion);

    Procedure ordenarInsercion(var V:TVector; ultimo:tipoPosicion);

implementation

    Procedure crearEstructuraVacia(V:TVector; var ultimo:tipoPosicion);
        begin
            ultimo := _POS_NULA; //ultimo es declarado dentro del programa principal
        end;
    //end crearEstructuraVacia
    Procedure insertarElemento(var V:TVector; var ultimo:tipoPosicion; elem:tipoBase);
        begin
            if (ultimo = Max) then
                writeln('Error, no hay espacio en la estructura')
            else begin
                inc(ultimo);
                V[ultimo] := elem;
            end;
        end;
    //end insertarElemento
    Function  recuperarElemento(var V:TVector; ultimo:tipoPosicion; pos:tipoPosicion):tipoBase;
        begin
            recuperarElemento := V[pos];
        end;
    //end recuperarElemento
    Function  primerElemento(V:TVector; ultimo:tipoPosicion):tipoPosicion;
        begin
            primerElemento := _MIN;
        end;
    //end primerElemento
    Function  ultimoElemento(V:TVector; ultimo:tipoPosicion):tipoPosicion;
        begin
            ultimoElemento := ultimo;
        end;
    //end ultimoElemento
    Procedure eliminarElemento(var V:TVector; var ultimo:tipoPosicion; pos:tipoPosicion);
        begin
            if ultimo = _POS_NULA then
                writeln('Error, la estructura esta vacia ')
            else begin
                V[pos] := V[ultimo];
                dec(ultimo);
            end;
        end;
    //end eliminarElemento
    Procedure buscarElemento(V:TVector; ultimo:tipoPosicion; elem:tipoBase; var enc:boolean; var pos: tipoPosicion);
        var
            i :tipoIndice;
        begin
            enc := false; 
            i := _MIN;
            pos := _POS_NULA;
            Repeat
                if (elem.clave = V[i].clave) then begin
                    enc := true;
                    pos := i;
                end;
                inc(i);
            until enc or (i> ultimo);
        end;
    //end buscarElemento
    Procedure ordenamientoBurbuja(var V:TVector; ultimo:tipoPosicion);
        var
            pasadas, i :tipoPosicion;
            Aux :tipoBase;
        begin
            for pasadas := 1 to (ultimo - 1) do
                for i := 1 to (ultimo - pasadas) do
                    if (V[i].clave > V[i+1].clave) then begin
                        Aux := V[i];
                        v[i] := V[i+1];
                        v[i+1] := Aux;
                    end;//<-- cierra el siclo if, for i, for pasadas
                //end-for
            //end-for
        end;
    //end ordenamientoBurbuja
    Procedure ordenamientoBurbujaMejorado(var V:TVector; ultimo:tipoPosicion);
        var
            pasadas, i :tipoPosicion;
            Aux :tipoBase;
            NoIntercambio :boolean;
        begin
            pasadas := 1;
            Repeat
                NoIntercambio := true;
                for i := 1 to (ultimo - pasadas) do
                    if (V[i].clave > V[i+1].clave) then begin
                        Aux := V[i];
                        v[i] := V[i+1];
                        v[i+1] := Aux;
                        NoIntercambio := false;
                    end;
                    inc(pasadas);
                //end for i
            until NoIntercambio;
        end;
    //end ordenamientoBurbujaMejorado
    Function  posMayor(V:TVector; ultimo:tipoPosicion):tipoPosicion;
        var
            maximo, i:tipoPosicion;
        begin
            maximo := _MIN;
            for i := SUCC(_MIN) to ultimo do
                if v[i].clave > v[maximo].clave then maximo := i;
            //end for
            posMayor := maximo;
        end;
    //end posMayor
    Procedure seleccion(var V:TVector; ultimo:tipoPosicion);
        var
            i, mayor :tipoPosicion;
            Aux :tipoBase;
        begin
            for i := ultimo downto 2 do begin
                mayor := mayor(v,i);
                Aux := V[mayor];
                v[mayor] := V[i];
                v[i] := Aux;
            end;
        end;
    //end seleccion
    Procedure busquedaBinaria(V:TVector; ultimo:tipoPosicion; elem:tipoBase; var enc:boolean; var pos:tipoPosicion);
        var p, u, m :tipoPosicion;
        begin
            p := _MIN;
            u := ultimo;
            enc := false;
            Repeat
                m := (p + u) div 2;
                if (V[m].clave = elem.clave) then begin
                    enc := true;
                    pos := m;
                end else
                    if (elem.clave > v[m].Clave) then
                        p := m + 1
                    else
                        u:= m - 1;//<-- cierra ciclos if
                    //end if
                //end if
            until enc or (p > u);
            if not enc then pos := p;
        end;
    //end busquedaBinaria
    Procedure insertarOrdenado(var V:TVector; var ultimo:tipoPosicion; elem:tipoBase; pos:tipoPosicion);
        var
            k :tipoPosicion;        
        begin
            if (ultimo < _MAX) then begin
                k := ultimo +1;
                Repeat
                    V[k] :=V[k -1];
                    k := k -1;
                until (k = pos);
                V[pos] := elem;
                ultimo := ultimo +1;
            end else
                writeln('La estructura esta llena');
            //end if
        end;
    //end insertarOrdenado
    Procedure desplazar(var V:TVector; i:tipoPosicion; elem:tipoBase; var NuevaPos:tipoPosicion);
        var
            enc :boolean;
        begin
            enc := false;
            while not enc and ( i > 1) do
                if (V[i-1].clave < elem.clave) then
                    enc:=true
                else begin
                    v[i] := v[i -1];
                    i := i -1;
                end;//end if and end while
            //end while
            NuevaPos := i;
        end;
    //end desplazar <---- procedimiento privado
    Procedure ordenarInsercion(var V:TVector; ultimo:tipoPosicion);
        var
            i, NuevaPos:tipoPosicion;
            Aux :tipoBase;
        begin
            For i := (_MIN +1) to ultimo do begin
                Aux := V[i];
                desplazar(V, i, V[i], NuevaPos);
                V[NuevaPos] := Aux;
            end;    
        end;
    //end ordenarInsercion
end.