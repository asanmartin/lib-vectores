unit tipoRegistro;

interface
    uses
        {Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
        Dialogs, ComCtrls, StdCtrls, ExtCtrls, Menus;}

    Type
        TRegistro = record
            clave :integer;
            titulo :string;
            editorial :string;
            tema :string;
            autor :string;
            fecha :Tdate;
            formato :string;
            adicionales :string;
            comentario :string;
            valor :real;
        end;
    
    var
        R:TRegistro; //<-- no creo sea necesario

implementation


end.